<?php

require('prefs.inc.php');
require('authentication.inc.php');
//$link = mysqli_connect($dbServer, $dbUser, $dbPass, $dbName);
$link = new mysqli($dbServer, $dbUser, $dbPass, $dbName);

$output = array(); //an array to hold our json key-value pairs

//must be authenticated
if(!$authenticated){
    
        errorHandler("not authenticated");
        // todo: anonymous url shortening 
    
}else{
    
    if (!isset($_GET['a'])) {//do we have a query string action
        
        errorHandler("query string action missing");
        //todo: return these errors to client in json
        
    }else{

        $action = $_GET['a'];
        
        switch ($action) {
            
            //return the keyword/item list
            case "itemlist":

                //todo: pagination
                        
                $query = "SELECT Items.id, Items.keyword, Items.clicks, Destinations.url FROM Items LEFT JOIN Destinations ON Items.dest_id = Destinations.id ORDER BY id DESC";
                        
                $result = runQuery($query, $dbServer, $dbUser, $dbPass, $dbName, $link);
                    
                //did we get a result?    
                $row_cnt = mysqli_num_rows($result);
                if ($row_cnt < 1) {
                    errorHandler("item list is empty");
                }else{
                    //load the list into the output array
                    while ($row = $result->fetch_assoc()) {
                        $output[] = array(
                            'id' => $row['id'],
                            'shorturl' => $row['keyword'],
                            'url' => $row['url'],
                            'clicks' => $row['clicks']
                        );
                    }

                }
            
                returnJSON($output); 
                break;
                //end return keyword/itemlist

            
            //add an item
            case "itemadd":
            
                //check the url
                //todo: better validation
                //todo: check url formation - maybe we don't want to allow https, etc.
                //todo: test load url and get page title?
                if (!isset($_GET['u'])) {
                    errorHandler("url missing");
                }else{
                    $url = $_GET['u'];
                    //make sure this is really an url
                    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $url)) {
                        errorHandler("malformed url");
                    }
                }
            
    
                //check the keyword
                //todo: better validation
                //todo: keyword filtering, reserved words, bad language...            
                if (!isset($_GET['k']) || $_GET['k']=="" || strlen($_GET['k']) < 5) {
                    
                    //keyword missing or too short, create random keyword
                    $keyword = randomKeyword($randomKeywordLength);
                    $customKeyword = false;
                    
                }else{//use the custom keyword
                    
                    $keyword = $_GET['k'];
                    //make sure it is alphanumeric with - or .
                    if (!preg_match("/^[A-Za-z0-9-.]+$/i", $keyword)) {
                        errorHandler("malformed url");
                    }                    
                    $customKeyword = true;   
                }
                        
                //proceed with the item add
                if(!isset($url) && !isset($keyword)){
                    
                    errorHandler("missing url and/or keyword");
                    
                }else{
                
                    //check for duplicate of keyword
                    $query = "SELECT id FROM Items WHERE keyword='$keyword'";
                    $result = runQuery($query, $dbServer, $dbUser, $dbPass, $dbName, $link);
                    $row_cnt = mysqli_num_rows($result);
                    //this keyword is already in db
                    if ($row_cnt > 0) { 
                        errorHandler("keyword already in database");
                    }
                    
                    //it's not duplicate, so let's do insert
                    
                    //have to insert URL first so we know its id
                    //we need details not returned by the query
                    //function at the moment, so doing query here
                    //todo: plan for handling duplicate URLs 
                    
                    $link = mysqli_connect($dbServer, $dbUser, $dbPass, $dbName);

                    //check db connection
                    if (mysqli_connect_errno()) {
                        die("Connect failed: %s\n". mysqli_connect_error());
                    }
                    
                    $url = mysqli_real_escape_string($link, $url);//escape the url
                    $query = "INSERT INTO `Destinations` (`url`) VALUES ('$url')";
                    $result = mysqli_query($link, $query) or die($query."<br/><br/>".mysqli_error($link));
                    
                    $dest_id = mysqli_insert_id($link);//get id of last insert
                    
                    //now add the new item to the item table
                    //escape keyword, even though it should already be okay
                    $keyword = mysqli_real_escape_string($link, $keyword);
                    $query = "INSERT INTO `Items` (`user_id`,`keyword`,`dest_id`) VALUES ('$userid','$keyword','$dest_id')";
                    $result = runQuery($query, $dbServer, $dbUser, $dbPass, $dbName, $link);
                        
                    //todo: log activity of this registered user
                    //todo: check for abuse, robots, etc.
                                
                    //build and send response
                    $output["shorturl"] = $shortURLRoot . $keyword;
                    returnJSON($output);    
            
            }
            //end of item add procedure
            
            break;
            
   
        //delete an item
        case "itemdelete":
            
            //did we get an item id?
            if (!isset($_GET['i'])) {
                
                $output["result"] = 'fail';
                //todo: should be returning json message like this for all the errors
                //errorHandler("didn't get an item id");
            
            }else{
                
                //todo: does this user really own this item?
                $delItem = $_GET['i'];
                if(!is_int(intval($delItem))){//did we get an int?
                    errorHandler("bad item id");
                }
                
                //need query details not returned by query function yet
                //so doing query here instead of function
                $link = mysqli_connect($dbServer, $dbUser, $dbPass, $dbName);

                //check db connection
                if (mysqli_connect_errno()) {
                    die("Connect failed: %s\n". mysqli_connect_error());
                }

                $query = "DELETE Items.*, Destinations.* FROM Items, Destinations WHERE Items.id = $delItem AND Destinations.id = Items.dest_id";
                
                //todo: if we do destination url reuse, need to check
                //destination is not used by another item before we delete it

                $result = mysqli_query($link, $query) or die($query."<br/><br/>".mysqli_error($link));
                
                $rows = mysqli_affected_rows($link);
                
                //as built at the moment, 2 rows should be the result
                if($rows==2) {
                    
                    $output["result"] = "success";
                    $output["itemid"] = $delItem;
                    
                }else{
                    
                    $output["result"] = "fail";
                    
                }
                               
            }
            returnJSON($output);
            break;
            //end of item delete

    
        //update item
        case "itemupdate":
            break;
        
        //display single item stats    
        case "iteminfo":
            break;
            
        }
        //end of switch

        }//end do we have a query string action

    }//end is authenticated


//FUNCTIONS
                   
function errorHandler($message) {
    
    //todo: should be returning json data for all these error messages.
    echo "Error: $message<br>";
    exit();

}


function runQuery($query, $dbServer, $dbUser, $dbPass, $dbName, $link) {
    
    //check db connection
    if (mysqli_connect_errno()) {
        die("Connect failed: %s\n". mysqli_connect_error());
    }

    $result = mysqli_query($link, $query) or die($query."<br/><br/>".mysqli_error($link));
    
    return $result;
}


function randomKeyword($randomKeywordLength){
    $wordlen = 5;
    $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    $randKeyword = '';
     for ($i = 0; $i < $randomKeywordLength; $i++) {
          $randKeyword .= $characters[rand(0, strlen($characters) - 1)];
     }
    return $randKeyword;
}


function returnJSON($output){//json response

    $ourJson = json_encode($output);
    //header("Content-type: application/json");
    echo $_GET['callback'] . '(' . $ourJson . ')';

}

                   
?>