<?php

//todo: don't allow anyone to submit this script's own url - endless loop

require('prefs.inc.php');

//PREPARE FOR REDIRECT

//did we get a keyword at all?
if (!isset($_GET['k'])) {
    errorHandler("keyword missing");
}

$keyword = $_GET['k'];

//only allow keywords with alphanumeric, period, dash    
if(!preg_match('/^[a-z0-9.\-]+$/i', $keyword)){
    errorHandler("malformed keyword");
}

//look for keyword in database
$query = "SELECT url FROM Destinations LEFT JOIN Items ON Items.dest_id = Destinations.id WHERE Items.keyword='$keyword'";
$result = runQuery($query, $dbServer, $dbUser, $dbPass, $dbName);

//did we get something other than 1 result?
$row_cnt = mysqli_num_rows($result);
if ($row_cnt != 1) {
    errorHandler("keyword missing or duplicated in database ");
}

//we have our destination url
while ($row = $result->fetch_assoc()) {
    $url = $row['url'];
}

//SIMPLE LOGGING

//LOGGING

/* Going to do logging here, but would like to keep
this go.php as lean as possible. Need to see if this
can be done asynchronously. */

//get the id of the item/keyword that was used
$query = "SELECT id FROM Items WHERE keyword='$keyword'";
$result = runQuery($query, $dbServer, $dbUser, $dbPass, $dbName);

//did we get a result?
$row_cnt = mysqli_num_rows($result);
if ($row_cnt != 1) {
    //can't find item id based keyword
}else{

    while ($row = $result->fetch_assoc()) {
        $id = $row['id'];
    } 
    
    //basic click logging
    $query = "UPDATE Items SET clicks = clicks + 1 WHERE id='$id'";
    runQuery($query, $dbServer, $dbUser, $dbPass, $dbName);
    
    //By getting timestamp, ip, and item id, we can
    //do some fancy analytics later on.    
   
    $ip = $_SERVER['REMOTE_ADDR'];//get client ip
    $stamp = date("Y-m-d H:i:s");//get time stamp
    $query = "INSERT INTO `go_stats` (`stamp`,`item_id`,`client_ip`) VALUES ('$stamp','$id','$ip')";    
    runQuery($query, $dbServer, $dbUser, $dbPass, $dbName);
}

//REDIRECT
header( 'Location: '.$url ) ;

//FUNCTIONS
                   
function errorHandler($message) {
    echo "Error: $message<br>";
    exit();
}

function runQuery($query, $dbServer, $dbUser, $dbPass, $dbName) {
    $link = mysqli_connect($dbServer, $dbUser, $dbPass, $dbName);

    //check db connection
    if (mysqli_connect_errno()) {
        die("Connect failed: %s\n". mysqli_connect_error());
    }

    $result = mysqli_query($link, $query) or die($query."<br/><br/>".mysqli_error($link));
    
    mysqli_close($link);

    return $result;
}

?>