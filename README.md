# URL Shrinker and Manager
## Summary
Database-driven, RESTful API web app to shorten and manage URLs. PHP and MySQL.

## Status
Close to finishing inital set of features. Item display and management in progress.

## Proposed Vision
- Single-page application.
- Takes a URL and returns a shorter one. 
- Ability to edit and delete URLs. 
- Ability to have random or custom URLs.
- RESTful API for performing the above tasks.
- Basic analytics for clickthroughs.

## Accomplished
- Front-end functions plan.
- Back-end functions plan.
- DB schema.
- DB and tables created.
- Redirect shortened URL.
- Create and add new item.
- Custom and random keywords.
- Apache .htaccess to remove .php extension and allow /keyword URL format.
- Messaging system for web interface.
- Display item list.
- Delete item.
- Simple stats.
- Underpinnings for fancy stats.


## TO DO
- Edit user item in web interface.
- More robust validation with helpful errors.
- User registration and accounts.
- Anonymous item add.
- Friendly messages for more error states.
- Item list pagination.
- Return errors to client as json data.
- Manage web interface state with hashtag variables.


