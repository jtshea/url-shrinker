# Tables Plan

## Items
- id
- user id
- keyword (shortened url - always unique)
- destination id
- status (0 active / 1 inactive / 2 deleted)
- custom flags (flags for special options like "you're leaving our site..." page)

## Destinations (actual URLs)
- id
- url
- status (0 good, 1 fail)
- timestamp last status audit
- duration of current status

## Users (will need salted hash)
- id
- email
- username
- pw
- lastlogon

## URL Activity Log
- timestamp
- item id
- client details (ip, etc.)

## System User Activity log
- timestamp
- user id
- client details (ip, etc.)
- item id
- action (add, delete, modify, activate, deactivate)

## DOS Prevention - only most recent 10 minutes, check frequency from same ip or referer
- timestamp
- ip
- referer


