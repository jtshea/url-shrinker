var dataArray = new Array(); //an array to hold the json data as variables
var formID = "#mainform";
var serverApp = "http://sheaworks.com/shrink/";

$(document).ready(function() {	
 	$.ajaxSetup({ cache: false }); //turn off ajax caching or we'll get the same result every time  	
	initialize(); //initialize the app    
});


function initialize(){
    
    //bind form submit
    $( formID ).submit(function( event ) {
        itemAdd();
        event.preventDefault();
    });
    
    //bind button
    $( "#listNav" ).bind( "click", function() {
        $( "#messages" ).hide();
        itemList();
    });
    
    //bind button
    $( "#addNav" ).bind( "click", function() {
        $( "#messages" ).hide();
        $( "#itemList" ).hide();
        $( "#addURLForm" ).show();    
    });

}

/* call json*/
function getJson(params,jcallback,datastyle){
  var apiURL = serverApp+"url-shrinker?callback=?";
  $.getJSON( apiURL,params,function( data ) {
      console.log( "success" );
    })
        .done(function(data) {
            console.log( "done" );
            
            switch(datastyle) {
            case "key-value":
                $.each( data, function( key, val ) {//put json in array
                    dataArray[key] = val;
                });                
                break;
            case "json":
                break;
            }
      
            jcallback(data);
        })
      .fail(function() {
        console.log( "error" );
      })
      .always(function() {
        console.log( "complete" );
      });
            
}

function itemAdd(){//item add
    
    var ourMessages=[]//message array
    var err = false;//error flag
    
    //grab all the field values
    var thePw="";
    var theUrl="";
    var theKeyword="";
    
    thePW = document.getElementById('p').value;
    theUrl = document.getElementById('u').value;
    theKeyword = document.getElementById('k').value;
    
 
        if(thePW==""){ourMessages.push("Password is blank.");err=true;}
        if(theUrl==""){ourMessages.push("URL is blank.");err=true;}
        if(theKeyword != "" && theKeyword.length < 5){ourMessages.push("Keywords need at least 5 characters.");err=true;}
        if(err==false){//no errors, query the server
            var jsonParams = "p="+thePW+"&a=itemadd&u="+theUrl+"&k="+theKeyword;
            getJson(jsonParams,itemAddCallback,"key-value");//get server response
        }

   if(ourMessages.length>0){message(ourMessages,"array")} 
}

function itemAddCallback(data){
    $submittedUrl = document.getElementById('u').value;
    //need to escape this
    $result = "<span class='origURL'>"+$submittedUrl+"</span><br/>has been shortened to <a href='"+dataArray['shorturl']+"' target='_blank'>"+dataArray['shorturl'] + "</a><br/><br/>";
    document.getElementById('u').value = "";
    document.getElementById('k').value = ""; 
    message($result,"html");    
}



function itemList(){
        
    $( "#addURLForm" ).hide();    
    $( "#itemList" ).show();
    
    var ourMessages=[]//message array
    var err = false;//error flag
    var thePw="";
    thePW = document.getElementById('p').value;
    
    if(thePW==""){ourMessages.push("Password is blank.");err=true;}
    
    if(err==false){//no errors, query the server
        var jsonParams = "p="+thePW+"&a=itemlist";
        getJson(jsonParams,itemlistCallback,"json");//get server response
    }
    if(ourMessages.length>0){message(ourMessages,"array")}
           
}



function itemlistCallback(data){
    var itemListHtml = "<table class='itemListTable'>";
    itemListHtml += "<tr><th>keyword</th><th>url</th><th>hits</th><th>delete</th></tr>";
    
    for(var prop in data){
        var delParams = data[prop].id+",'"+data[prop].shorturl+"','"+data[prop].url+"'"; 
        itemListHtml += "<tr id='listitem"+data[prop].id+"'><td>"+data[prop].shorturl+"</td><td>"+data[prop].url+"</td><td>"+data[prop].clicks+"</td>";
        itemListHtml += "<td><span class=\"delItemButton\" onClick=\"deleteItem("+delParams+")\">delete</span></td>";
        itemListHtml += "</tr>";
    }
    itemListHtml += "</table>";
    $( "#itemListContent" ).html(itemListHtml);
    //todo: multi-select
}





function deleteItem(id,shorturl,url){
    if(confirm('Delete '+shorturl+" ("+url+")?")){
        
        //call delete
        var ourMessages=[]//message array
        var err = false;//error flag
        var thePw="";
        thePW = document.getElementById('p').value;

        if(thePW==""){ourMessages.push("Password is blank.");err=true;}
        if(err==false){//no errors, query the server
            
            var jsonParams = "p="+thePW+"&a=itemdelete&i="+id;
            getJson(jsonParams,deleteItemCallback,"json");//get server response
        }
        if(ourMessages.length>0){message(ourMessages,"array")}

    }else{
        message("Delete of "+shorturl+" canceled.","array");
    }
}

function deleteItemCallback(data){
    if(data.result=="success"){
        $("#listitem"+data.itemid).css("background-color", "red");
        $("#listitem"+data.itemid).hide(1000);
        message("Deleted item id " + data.itemid,"html");
    }else{
        message("Delete failed.","html");   
    }
}


function editURL(){
    
}



function logonUser(){

}


function logoutUser(){
    
}


function registerUser(){
    
}


//internal functions

//set / get session cookie or url token
function token(){
    
}

//get user details, ip, etc.
function user(){
    
}

//maybe
function captcha(){
    
}
    
function message(message,type){
    $( "#messages" ).hide();
    $( "#messages" ).html("");
    
    switch(type) {
    case "array":
            
        var arrayLength = message.length;
        for (var i = 0; i < arrayLength; i++) {//loop over new messages
            $( "#messages" ).append( "<span class='msg'>" + message[i] + "</span>" )
        }
        break;
            
    case "html":
        $( "#messages" ).html(message);
        break;

    }
    window.scrollTo(0, 0);   
    $( "#messages" ).show();
}


